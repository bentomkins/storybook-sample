import { tw as tailwind } from "tailwind-styled-components";

const Button = tailwind.button`
    flex
    items-center
    justify-center
    flex-col
    w-full
    bg-indigo-100
  `;

  export default Button;

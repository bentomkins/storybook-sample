import React from 'react'
import Button from './Button'
import "../../index.css"

export default {
  title: 'SkillsLounge/Button',
  component: Button
}

export const DefaultButton = () => <Button disabled onClick={()=>console.log("default click")}>Default Button</Button>

import tw from "tailwind-styled-components";

const Button = tw.button`
    flex
    items-center
    justify-center
    flex-col
    w-full
    bg-indigo-100
  `;

  export { Button };

import { tw as Tailwind } from "tailwind-styled-components";

const Button = Tailwind.button`
    flex
    items-center
    justify-center
    flex-col
    w-full
    bg-indigo-100
  `;

  export default Button;

import React, { forwardRef } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { darken, rgba, opacify } from "polished";
import tw from "tailwind-styled-components";

const Button = () => {
  const Container = tw.div`
    flex
    items-center
    justify-center
    flex-col
    w-full
    bg-indigo-200
  `;

  return (
    <Container>
      <div>Use the Container as any other React Component</div>
    </Container>
  );
};
export default Button;

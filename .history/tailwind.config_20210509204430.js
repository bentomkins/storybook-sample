// tailwind.config.js
module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        electric: '#A9A9A9',
        ribbon: '#F8F8F8',
      },
    },
  },
  variants: {
    extend: {opacity: ['disabled'],},
  },
  plugins: [],
}
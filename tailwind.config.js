// tailwind.config.js
module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        electric: '#E8E8E8',
        ribbon: '#F8F8F8',
        gray: {
          button: '#EAEAEA',
          shadow: '#909090',
        },
      },
      fontFamily: {
        'sans': ['Montserrat']
      },
    },
  },
  variants: {
    extend: {opacity: ['disabled'],},
  },
  plugins: [],
}
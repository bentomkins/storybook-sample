import React from 'react'
import { Story } from '@storybook/react/types-6-0'
import { EnvBtnComponent } from './EnvironmentButton'

export default {
  title: 'Common/Environment Button',
  component: EnvBtnComponent,
  argTypes: {
    status: {
      description: 'TODO: Add description',
      control: {
        type: 'select',
        options: ['failed', 'loading', 'loaded'],
      },
    },
    small: {
      description: 'TODO: Add description',
      type: 'boolean',
    },
    active: {
      description: 'TODO: Add description',
      type: 'boolean',
    },
    children: {
      description: 'The button content',
      defaultValue: 'Button',
      type: { name: 'display text', required: true },
    },
  },
}


const Template: Story = props => <EnvBtnComponent {...props} />
export const EditorButton = Template.bind({})
EditorButton.args = { status: 'failed', small: true, active: true, children: 'console' }


import tw, { styled, css } from 'twin.macro';
import { keyframes } from 'styled-components'
import { LoadingIndicator } from '../LoadingIndicator/LoadingIndicator';

// add tailwind twin vscode extension for highlighting and autocomplete:
// https://marketplace.visualstudio.com/items?itemName=lightyen.tailwindcss-intellisense-twin

export interface IBorderProps {
  active?: boolean;
  relative?: boolean;
  small?: boolean;
}

const fade = keyframes`
  0%   { opacity:0.4; }
  100% { opacity:1; }`
const fadeIn = css`animation: ${fade} 2s 1`

const Border = styled.div(({ active, relative, small }: IBorderProps) => [
  tw`inline-block m-0 p-2.5 rounded-md`,
  active ? [tw`border border-gray-800 border-dashed`, fadeIn] : tw`border-transparent`,
  relative ? [tw`relative`] : css``,
  small ? tw`rounded p-1.5` : tw``,
]);

export interface IEnvironmentButtonProps extends IBorderProps {
  children?: string;
  status?: 'failed' | 'loading' | 'loaded';
  small?: boolean
}

export const EnvironmentButton = styled.button(({small}: IEnvironmentButtonProps) => [
  tw`bg-gray-300 px-2.5 py-1.5 uppercase truncate rounded text-lg`,
  small ? tw`px-1.5 py-0.5 text-sm`: tw``,
]);


export const EnvBtnComponent = ({active, small, status, children}: IEnvironmentButtonProps) => {
  return (
    <Border active={active} relative small={small}>
      <LoadingIndicator absolute status={status} small={small}></LoadingIndicator>
      <EnvironmentButton small={small}>{children}</EnvironmentButton>
    </Border>
  )};

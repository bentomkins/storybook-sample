import React from 'react'
import SuperscriptText from '../SuperscriptText/SuperscriptText'


const InfoBox = (props) => {
  const { 
    mainTextVariants='w-4/7', 
    minorTextVariants='w-2/7',
    iconVariants='w-1/7',
    superscriptText,
    minorElementText,
    thirdElement,
    children 
  } = props

  // console.log(secondElement)

  return ( 
    <div className='
      flex 
      border
      border-gray-300
      shadow-lg         
      relative
      rounded-lg
      px-8 py-2 
      '>
        <div className={`${mainTextVariants}`}>
          {children}
        </div>

        <SuperscriptText>{superscriptText}</SuperscriptText>

        <div className={`${minorTextVariants} absolute right-10 text-lg`} >
          {minorElementText}
        </div>
        <div className={`${iconVariants} absolute right-2`} >
          {thirdElement}
        </div>
      </div>
  )
}

export default InfoBox;
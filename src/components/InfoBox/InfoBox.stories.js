import React from 'react'
import InfoBox from './InfoBox'
import "../../index.css"
import { ClockIcon, RocketIcon } from "@primer/octicons-react";
import SuperscriptText from '../SuperscriptText/SuperscriptText';

export default {
  title: 'SkillsLounge/InfoBox',
  component: InfoBox
}

export const InfoBoxDefault = () => <InfoBox 
  // superscriptText='Hands On With Jenkins 101'
  minorElementText='10:48'
  thirdElement={<ClockIcon size={24} />}
  >
  02. Configure Jenkins
  </InfoBox>

export const InfoBoxAnother = () => <InfoBox 
  // superscriptText='Default Superscript Text'
  minorElementText='Time Is Running'
  thirdElement={<RocketIcon size={24} />}
  >
  <SuperscriptText>Default Superscript Text</SuperscriptText>
  Another InfoBox Example
  </InfoBox>
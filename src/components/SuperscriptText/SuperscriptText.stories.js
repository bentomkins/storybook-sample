import React from 'react'
import SuperscriptText from './SuperscriptText'
import "../../index.css"


export default {
  title: 'SkillsLounge/SuperscriptText',
  component: SuperscriptText
}

export const DefaultSuperscriptText = () => <SuperscriptText>Default SuperscriptText</SuperscriptText>

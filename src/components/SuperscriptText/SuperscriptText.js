import React from 'react'

const SuperscriptText = (props) => {
  const { children, variant='-left-3.5 -top-5' } = props
  return (
    <div className='relative'>
      <span className={`
        absolute 
        bg-white 
        px-2 
        text-sm
        ${variant}`}>{children}</span>
    </div>
  )
}

export default SuperscriptText
import React from 'react'
import ButtonNew from './ButtonNew'
import TrafficLight from '../TrafficLight/TrafficLight'
import "../../index.css"
import { TriangleLeftIcon, TriangleRightIcon  } from "@primer/octicons-react";

export default {
  title: 'SkillsLounge/ButtonNew',
  component: ButtonNew
}



export const DefaultButton = () => <ButtonNew disabled onClick={()=>console.log("default click")}>Default Button</ButtonNew>
export const ResizedButton = () => <ButtonNew variant='px-3 py-3'>Resized Button</ButtonNew>
export const ReadyButton = () => <ButtonNew><TrafficLight/>Ready Button</ButtonNew>
export const SteadyButton = () => <ButtonNew><TrafficLight variant='steady'/>Steady Button</ButtonNew>
export const GoButton = () => <ButtonNew><TrafficLight variant='go'/>Go Button</ButtonNew>
export const ButtonLeft = () => <ButtonNew><TriangleLeftIcon size={24}/></ButtonNew>
export const ButtonRight = () => <ButtonNew><TriangleRightIcon size={24}/></ButtonNew>

// export const Secondary = () => <ButtonNew><span className="absolute top-0 left-0 dot-inactive"/>Inactive Button</ButtonNew>
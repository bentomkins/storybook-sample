import React from 'react'

const ButtonNew = (props) => {

  const { variant = 'px-8 py-2', children, onClick, disabled } = props

  return (
    <button disabled={disabled} onClick={onClick} className={`
    disabled:opacity-30
    relative
    rounded-lg 
    bg-white
    border
    border-gray-300
    shadow-lg
    focus:outline-black
    ${variant}`} >
      {children}
    </button>
  )
}

export default ButtonNew;


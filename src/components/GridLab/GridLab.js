import React from 'react'
import Card from '../Card/Card'
import ButtonNew from '../ButtonNew/ButtonNew'

const GridLab = (props) => {

  const { labs } = props
  // const data = [ {},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]

  let chunked = []
  let size = 3;

  for (let i = 0;  i < labs.length; i += size) {
    chunked.push(labs.slice(i, i + size))
  }  
  console.log(chunked)
  chunked.map((row) => console.log(row.map((item)=>console.log(item))))
  return (

    chunked.map((row) => {return (
      // row.map((item)=> {
      //   return (
          <div className="flex space-y-1 space-x-4">
            <Card title={row[0].labName} description={row[0].description}>
              <ButtonNew variant='px-3 py-1 text-black text-xs'>START LAB</ButtonNew>
            </Card>
            {row[1] !== undefined ? 
            <Card title={row[1].labName} description={row[1].description}>
              <ButtonNew variant='px-3 py-1 text-black text-xs'>START LAB</ButtonNew>
            </Card> : <></> }
            {row[2] !== undefined ? 
            <Card title={row[2].labName} description={row[2].description}>
              <ButtonNew variant='px-3 py-1 text-black text-xs'>START LAB</ButtonNew>
            </Card> : <></> }
            
          {/* <div className="w-1/3 bg-blue-400 rounded-xl">w-1/3</div>
          <div className="w-1/3 bg-green-400 rounded-xl">w-1/3</div>
          <div className="w-1/3 bg-yellow-300 rounded-xl">w-1/3</div> */}
        </div>
        // )
      // })


    )
    })

  )
}

export default GridLab
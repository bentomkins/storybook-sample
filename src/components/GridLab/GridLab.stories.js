import React from 'react'
import GridLab from './GridLab'
import "../../index.css"
import data from './lab.json'


export default {
  title: 'SkillsLounge/GridLab',
  component: GridLab
}

export const DefaultGridLab = () => <GridLab labs={data}></GridLab>

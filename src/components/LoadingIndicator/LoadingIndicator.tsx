import tw, { styled, css } from 'twin.macro';
import { keyframes } from 'styled-components'

export interface ILoadingProps {
  status?: 'failed' | 'loading' | 'loaded';
  small?: boolean;
  absolute?: boolean;
}

const fade = keyframes`
  0%   { opacity:0.4; }
  100% { opacity:1; }`
const fadeIn = css`animation: ${fade} 2s 1`

const breatheAnimation = keyframes`
  0%   { opacity:1; }
  50%  { opacity:0.2; transform: scale(1.4)}
  100% { opacity:1; }`
const breathingLoop = css`animation: ${breatheAnimation} 7s infinite`


export const LoadingIndicator = styled.span(({ status, small, absolute }: ILoadingProps) => [
  tw`z-10 inline-block rounded-full`,
  absolute ? tw`absolute` : css``,
  small ? tw`w-3 h-3` : tw`w-4 h-4`,
  status === 'failed' && [tw`bg-red-500`, fadeIn],
  status === 'loading' && [tw`bg-yellow-500`, breathingLoop],
  status === 'loaded' && [tw`bg-green-500`, fadeIn],
]);

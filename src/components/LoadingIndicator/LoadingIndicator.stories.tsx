import React from 'react'
import { Story } from '@storybook/react/types-6-0'
import { LoadingIndicator, ILoadingProps } from './LoadingIndicator'
// import "../../index.css"

export default {
  title: `Common/Loading Indicator`,
  component: LoadingIndicator,
  argTypes: {
    status: {
      description: 'TODO: Add description',
      control: {
        type: 'select',
        options: ['failed', 'loading', 'loaded'],
      },
    },
    absolute: {
      description: 'TODO: Add description',
      defaultValue: false,
      type: 'boolean',
    },
    small: {
      description: 'TODO: Add description',
      defaultValue: true,
      type: 'boolean',
    },
  },
}

const Template: Story = props => <LoadingIndicator small {...props} />

export const Failed = Template.bind({})
Failed.args = { status: 'failed' } as ILoadingProps;

export const Loading = Template.bind({})
Loading.args = { status: 'loading' } as ILoadingProps;

export const Loaded = Template.bind({})
Loaded.args = { status: 'loaded' } as ILoadingProps;

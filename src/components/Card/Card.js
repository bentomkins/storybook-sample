import React from 'react'
import "./Card.css"
import { PlayIcon } from "@primer/octicons-react";

const Card = (props) => {

  const { variant, title, description, children } = props

  return (
    <div className={`labcard box-content h-40 w-96 border text-lg font-bold relative ${variant}`}>
      {variant === 'active' ? 
        <div className='absolute right-0 top-0 h-5 w-16 px-1 bg-red-600 text-sm'><PlayIcon/> LIVE </div> 
        : <></>

      }
      {/* <div>{title.toUpperCase()}</div> */}
      <div className='px-8 py-6'>
        <div className='h-8 font-mono'>{title.toUpperCase()}</div>
        <div className='h-14 text-xs text-white'>{description.substring(0,130)} ...</div>
        <div className='absolute h-8 w-80'>{children}</div>
      </div>
    </div>
    
  )

}

export default Card
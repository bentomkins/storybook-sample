import React from 'react'
import Card from './Card'
import "../../index.css"
import ButtonNew from '../ButtonNew/ButtonNew'
import data from './lab.json'

export default {
  title: 'SkillsLounge/Card',
  component: Card
}


// const labInfos = {data:[
//   {"types":[{"label":"Academy","value":"academy"}],"timer":{"label":"30 minutes","value":"30"},"basePorts":[],"resourcesCPUMEM":{"label":"256 / 1024 : (vCPU; units) / (Memory; MiB)","value":"256:1024"},"categories":[{"label":"Artifact Management","value":"artifactmanagement"}],"containerDefinition":[],"createdDateTime":1611851000,"uuid":"24b47b11-7eff-4d9a-96c0-05ffc07571be","description":"Ach2","labName":"Ach2","userUuid":"c9a5479a-47e8-40b0-8b66-647111dcb9a8","achievements":false,"achievementsDebug":false,"achievementsImage":"form","integratedWebsite":false,"integratedWebsiteURL":"","ide":true,"active":true,"shell":false,"baseGitUrl":""},
//   {"types":[{"label":"Academy","value":"academy"}],"timer":{"label":"30 minutes","value":"30"},"basePorts":[],"resourcesCPUMEM":{"label":"256 / 1024 : (vCPU; units) / (Memory; MiB)","value":"256:1024"},"categories":[{"label":"Artifact Management","value":"artifactmanagement"}],"containerDefinition":[],"createdDateTime":1611851000,"uuid":"7bdf7acb-2d69-41e0-ae4c-84ae85bfc6ca","description":"ach5","labName":"ach5","userUuid":"c9a5479a-47e8-40b0-8b66-647111dcb9a8","achievements":false,"achievementsDebug":false,"achievementsImage":"ach5","integratedWebsite":false,"integratedWebsiteURL":"","ide":true,"active":true,"shell":false,"baseGitUrl":""},
//   {"types":[{"label":"Academy","value":"academy"}],"timer":{"label":"30 minutes","value":"30"},"basePorts":[],"resourcesCPUMEM":{"label":"256 / 1024 : (vCPU; units) / (Memory; MiB)","value":"256:1024"},"categories":[{"label":"Artifact Management","value":"artifactmanagement"}],"containerDefinition":[],"createdDateTime":1611851000,"uuid":"bf05d1b1-df6e-4cec-a789-249fb8b0e88f","description":"ach7","labName":"ach7","userUuid":"c9a5479a-47e8-40b0-8b66-647111dcb9a8","achievements":false,"achievementsDebug":false,"achievementsImage":"busybox","integratedWebsite":false,"integratedWebsiteURL":"","ide":true,"active":true,"shell":false,"baseGitUrl":""}
// ]}

export const CardDefault = () => <Card title={data[9].labName}
  description={data[9].labName}
>
    <ButtonNew variant='px-3 py-1 text-black text-xs'>START LAB</ButtonNew>
</Card>

console.log(data)


export const ActiveCard = () => <Card 
  title='Hands on with Jenkins' 
  description='Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Ciceros De Finibus Bonorum et Malorum for use in a type specimen book.' 
  variant='active'>
    <ButtonNew variant='px-3 py-1 text-black text-xs'>OPEN LAB</ButtonNew>
    <span className='px-2'></span>
    <ButtonNew variant='px-3 py-1 text-black text-xs'>DESTROY</ButtonNew>
    <span className='absolute right-0 text-indigo-300' >10:48</span>
  </Card>
import {React, useState, useEffect} from 'react'
import Slides from './Slides'
import "../../index.css"
import ButtonNew from '../ButtonNew/ButtonNew'
import { TriangleLeftIcon, TriangleRightIcon  } from "@primer/octicons-react";

export default {
  title: 'SkillsLounge/Slides',
  component: Slides
}


const handleChangeClickNext = () => { console.log("Next") }
const handleChangeClickPrev = () => { console.log("Prev")}

export const Default = () => (
<>
<div className="h-8 py-2 text-white text-center rounded-t-lg  bg-purple-900">
🙂  🙃  🙂  🙂  🙂  🙂
</div>  
<Slides/>
<div className="bg-purple-900 rounded-b-lg px-2 py-2" >
<span className="bottom-5 left-5">
  <ButtonNew variant='px-3 py-1' onClick={()=>handleChangeClickPrev()}>
    <TriangleLeftIcon size={24}/>
  </ButtonNew>
</span>
<span className="float-right bottom-5 right-5">
<ButtonNew variant='px-3 py-1' onClick={()=>handleChangeClickNext()}>
  <TriangleRightIcon size={24}/>
</ButtonNew>
</span>
</div>
</>
)
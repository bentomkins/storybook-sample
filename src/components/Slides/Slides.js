import { React, useEffect, useState } from 'react'

import ButtonNew from '../ButtonNew/ButtonNew'
import { TriangleLeftIcon, TriangleRightIcon  } from "@primer/octicons-react";
import ReactMarkdown from 'react-markdown'
import Zoom from 'react-medium-image-zoom'
import './Slides.css'

const Slides = (props) => {

  const [ current, setCurrent ] = useState(4)
  const [ slides, setSlides ]= useState([])

  const readme = `# TEST

  ---
  
  # 1. Goals and description
  
   1. How to create a Cloudformation stack from a template
  
   2. How to add outputs
  
   3. How to update resources
  
   4. How are parameters supported
  
   5. What is auto-rollback
  
   6. What is Drift Detection
  
   7. How to clean upresoruces
  
  ---
  
  # 2. Administration
  
  This lab requires you to have access to an AWS account that is not provided in this lab.
  
  The majority of our lab actions will be done from the AWS management console.
  
  SkillsLounge will provide the base code for us to upload to AWS.
  
  We will make updates to this file via the AWS in-built designer.
  
  ![](CloudFormation0.png)
  
  ---
  
  # 3. Introduction
  
  For this lab, we will explore how to use CloudFormation templates in the AWS management console to create infrastructure.
  
  We will be creating an EC2 instance and a Security Group to manage connections to it.
  
  We will introduce parameters and outputs and several other features of CloudFormation.
  This lab requires you to have access to an AWS account that is not provided in this lab.
  
  The majority of our lab actions will be done from the AWS management console.
  
  SkillsLounge will provide the base code for us to upload to AWS.
  
  We will make updates to this file via the AWS in-built designer.
  
  ![](https://i.imgur.com/3jQXR48.png)
  This lab requires you to have access to an AWS account that is not provided in this lab.
  
  The majority of our lab actions will be done from the AWS management console.
  
  SkillsLounge will provide the base code for us to upload to AWS.
  
  We will make updates to this file via the AWS in-built designer.
  
  ![](https://i.imgur.com/3jQXR48.png)
  This lab requires you to have access to an AWS account that is not provided in this lab.
  
  The majority of our lab actions will be done from the AWS management console.
  
  SkillsLounge will provide the base code for us to upload to AWS.
  
  We will make updates to this file via the AWS in-built designer.
  
  ![](https://i.imgur.com/3jQXR48.png)
  
  `


  useEffect(() => {
    console.log()
    setSlides(readme.trim().split("---"))
    return () => setCurrent(current)
  }, props)

  // const clickRight = () => {console.log(slides)}
  // const clickLeft = () => {console.log(current)}

  const handleChangeClickNext = () => { if (current < slides.length) setCurrent(current + 1)}

  const handleChangeClickPrev = () => { if (1 < current) setCurrent(current - 1)}

  // const DataPrint = (data) => {
  //   console.log(data)
  //   return <></>
  // }

  // const ImagesProcessing = (image) => {
  //   let src =  image.image
  //   return (
  //     <Zoom>
  //       <img
  //         alt={src}
  //         src={src}
  //         width="100%"
  //       />
  //     </Zoom>
  //   )
    
  // }

  return (

<div className="slides bg-purple-700 markdown px-2 py-2 text-white lg:h-40 md:h-30 sm:h-64">
  <ReactMarkdown 
        source={slides ? slides[current - 1] : ""} // eslint-disable-next-line
        renderers={{
          image: (props) => (
            // <DataPrint data={props}></DataPrint>
            // <Zoom>
              <img
                {...props}
                alt={props.src}
                style={{ maxWidth: "100%", maxHeight: "100%" }}
              />

          ),
        }}
        // transformImageUri={(uri) =>
        //   uri.startsWith("http" || "https")
        //     ? uri
        //     : uri.startsWith("/")
        //     ? `${AWS_BUCKET_URL}${session.session.labId}${uri}`.toLowerCase()
        //     : `${AWS_BUCKET_URL}${session.session.labId}/${uri}`.toLowerCase()
        // }
      />



    </div>



  )
}

export default Slides
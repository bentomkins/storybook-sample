import React from 'react';
import tw, { styled, css } from 'twin.macro';
import { keyframes } from 'styled-components'
import "./TrafficLight.css"

export interface ILoadingProps {
  status: 'failed' | 'loading' | 'loaded';
  small?: boolean;
  absolute?: boolean;
}

const fade = keyframes`
  0%   { opacity:0.2; }
  100% { opacity:1; }`
const fadeIn = css`animation: ${fade} 3s 1`

const breatheAnimation = keyframes`
  0%   { opacity:1; }
  50%  { opacity:0.2; }
  100% { opacity:1; }`
const breatheLoop = css`animation: ${breatheAnimation} 8s infinite`

export const LoadingIndicator = styled.span(({ status, small, absolute }: ILoadingProps) => [
  tw`top-0 left-0 rounded-full`,
  absolute ? tw`absolute` : css``,
  small ? tw`inline-block w-3 h-3` : tw`inline-block w-4 h-4`,
  status === 'failed' && [tw`bg-red-500`, fadeIn],
  status === 'loading' && [tw`bg-yellow-500`, breatheLoop],
  status === 'loaded' && [tw`bg-green-500`, fadeIn],
])


const TrafficLight = (props: any) => {

  const { variant = 'ready', position= '-left-8 -top-2 z-10' } = props

  return (
    <span tw='relative'>
      {/* <span className={`dot ${variant} ${position} absolute`}/> */}
      <svg>
        <circle cx="6" cy="6" r="6" fill="orange" />
      </svg>
    </span>
  )
}

export default TrafficLight;




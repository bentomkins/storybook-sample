import React from 'react'
import TrafficLight, { LoadingIndicator, Button2 } from './TrafficLight'
import "../../index.css"

export default {
  title: 'SkillsLounge/TrafficLights',
  component: TrafficLight
}

export const test = () => <LoadingIndicator status="loaded" small />


export const test2 = () => <span style={{
  position: 'relative',
  width: '50px',
  height: '50px',
  border: '1px solid orange',
  display: 'inline-block',
  }}>
  <LoadingIndicator status="loaded" absolute small />
</span>

export const Ready = () => <TrafficLight position='left-0 top-0'/>
export const Steady = () => <TrafficLight position='left-0 top-0' variant='steady'/>
export const Go = () => <TrafficLight position='left-0 top-0' variant='go'/>
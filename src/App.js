import logo from './logo.svg';
import './App.css';
import ButtonNew from './components/ButtonNew/ButtonNew'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <ButtonNew>Test</ButtonNew>
        <button class="hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-opacity-50">TEST</button>
      </header>
    </div>
  );
}

export default App;
